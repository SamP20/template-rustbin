import subprocess
from mako.template import Template

def initialize(info):
    subprocess.run(["cargo", "init", "--bin"], check=True)
    substitute_file("README.md", info)


def substitute_file(name, info):
    template = Template(filename=name)
    with open(name, "w") as fh:
        fh.write(template.render(**info))